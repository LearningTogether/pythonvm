#include "util/bufferedInputStream.hpp"
#include "code/binaryFileParser.hpp"
#include "runtime/interpreter.hpp"
#include "runtime/universe.hpp"
#include "memory/heap.hpp"

#include<iostream>
using namespace std;
struct input{
    int a;
    int b;
    input(int x, int y){
        this->a = x;
        this->b = y;
    }
};

struct output{
    int addi;
    int subt;
//    output(int x, int y){
//        this->addi = x;
//        this->subt = y;
//    }
};
void doubleTheValue(input sou, output res)
{
    res.addi = sou.a + sou.b;
    res.subt = sou.a - sou.b;
}

void doubleTheRef(input& sou, output& res)
{
    res.addi = sou.a + sou.b;
    res.subt = sou.a - sou.b;
}

void doubleThePoint(input* sou, output** res)
{

    output* result = new output;//指针的错误使用方法，只是改变了行参的指向地址
    // 谁申请，谁释放，不符合规范
    result->addi = sou->a + sou->b;
    result->subt = sou->a - sou->b;
    *res = result;
    delete result;

    /*
    res->addi = sou->a + sou->b;
    res->subt = sou->a - sou->b;
    */
}

int main(int argc, char** argv) {
    input sou(1, 2);
    // output des;
    output des;
    output* desPoint = &des;
    cout << "Value of a before calling double function = " << sou.a << sou.b << "\n";
    doubleThePoint(&sou, &desPoint);
    // delete desPoint;
    desPoint->subt = 0;
    cout<<desPoint->subt<<endl;
    cout << "Final Value of a =                          " << des.addi << des.subt << "\n";


    /*
    if (argc <= 1) {
        printf("vm need a parameter : filename\n");
        return 0;
    }

    Universe::genesis();
    BufferedInputStream stream(argv[1]);
    BinaryFileParser parser(&stream);
    Universe::main_code = parser.parse();
    Universe::heap->gc();

    Interpreter::get_instance()->run(Universe::main_code);
    */

    return 0;
}

